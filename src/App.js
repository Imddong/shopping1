import React,{Component} from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component{
  render(){
    return (
      <div className="gray-background">
       <img src={logo} lat="loco"/>
       <h2>let's develop shopping mall system!</h2>
      </div>
    );

  }
}

export default App;
